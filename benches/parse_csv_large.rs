#[macro_use]
extern crate criterion;
extern crate csv;

use std::io::Cursor;

use criterion::Criterion;
use csv::parse;

static INPUT: &'static str = r#"first,last,address,city,zip
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123
John,Doe,120 any st.,"Anytown, WW",08123"#;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("large", |b| {
        let mut input = Cursor::new(&INPUT);
        b.iter(|| parse(&mut input).unwrap())
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
