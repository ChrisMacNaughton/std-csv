#[macro_use]
extern crate criterion;
extern crate csv;

use std::io::Cursor;

use criterion::Criterion;
use csv::parse;

static INPUT: &'static str = r#""Once upon
a time",5,6,""quoted"",\"lol"#;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("line", |b| {
        let mut input = Cursor::new(&INPUT);
        b.iter(|| parse(&mut input).unwrap())
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
