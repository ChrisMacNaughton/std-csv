use std::io::BufRead;

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn it_parses_a_part() {
        let input = Cursor::new("part,another part");
        let mut parser = Parser::new(input);
        assert_eq!("part", parser.next_part().unwrap());
        assert_eq!("another part", parser.next_part().unwrap());
    }

    #[test]
    fn it_handles_escaped_quote() {
        let input = Cursor::new("\\\"");
        let mut parser = Parser::new(input);
        assert_eq!("\\\"", parser.next_part().unwrap());
    }

    #[test]
    fn it_handles_double_quote() {
        let input = Cursor::new(r#"ha "ha" ha"#);
        let mut parser = Parser::new(input);
        assert_eq!("ha \"ha\" ha", parser.next_part().unwrap());
    }

    #[test]
    fn it_handles_newline_in_row() {
        let input = Cursor::new("\"quoted\nnewline\"");
        let expected = "quoted\nnewline";
        let mut parser = Parser::new(input);
        assert_eq!(expected, parser.next_part().unwrap());
    }

    #[test]
    fn it_returns_a_line() {
        let input = Cursor::new("part,another part");
        let mut parser = Parser::new(input);

        assert_eq!(vec!["part", "another part"], parser.next().unwrap());
    }

    #[test]
    fn it_handles_multiple_lines() {
        let input_raw = r#"name,dob,email
test1,01-12-1980,test1@example.com
test2,20-2-1970,test2@example.com
"#;
        let input = Cursor::new(input_raw);
        let mut parser = Parser::new(input);
        let expected = vec![
            vec!["name", "dob", "email"],
            vec!["test1", "01-12-1980", "test1@example.com"],
            vec!["test2", "20-2-1970", "test2@example.com"],
        ];
        assert_eq!(expected[0], parser.next().unwrap());
        assert_eq!(expected[1], parser.next().unwrap());
        assert_eq!(expected[2], parser.next().unwrap());
        assert_eq!(None, parser.next());
    }
}

const QUOTE: u8 = b'"';
const NEWLINE: u8 = b'\n';
const LINEFEED: u8 = b'\r';

pub struct Parser<T: BufRead> {
    reader: T,
    eof: bool,
    quoted: bool,
    escaped: bool,
}

#[derive(Debug)]
enum ParseError {
    Eol,
    Eof,
}

impl<T: BufRead> Parser<T> {
    pub fn new(reader: T) -> Parser<T> {
        Parser {
            reader,
            eof: false,
            quoted: false,
            escaped: false,
        }
    }

    fn next_part(&mut self) -> Result<String, ParseError> {
        let (consumed, bytes, err): (usize, Vec<u8>, Option<ParseError>) = {
            let mut err = None;
            let mut buff = self
                .reader
                .fill_buf()
                .map_err(|_| ParseError::Eof)?
                .iter()
                .peekable();
            if buff.len() == 0 {
                err = Some(ParseError::Eof);
                self.eof = true;
            }
            let mut bytes = vec![];
            let mut consume = 0;
            if err.is_none() {
                self.quoted = match buff.peek() {
                    Some(c) => {
                        if **c == QUOTE {
                            consume += 1;
                            true
                        } else {
                            false
                        }
                    }
                    _ => false,
                };
                if let Some(s) = buff.peek() {
                    match **s {
                        NEWLINE | LINEFEED => {
                            consume += 1;
                            err = Some(ParseError::Eol);
                        }
                        _ => {}
                    }
                }
                for _ in 0..consume {
                    let _ = buff.next();
                }
                // consume = 0;
                if let Some(&&NEWLINE) = buff.peek() {
                    consume += 1;
                    err = Some(ParseError::Eol);
                }
                if err.is_none() {
                    let mut will_reset_escaped = false;
                    while let Some(byte) = buff.next() {
                        match byte {
                            b'"' => {
                                consume += 1;
                                if let Some(s) = buff.peek() {
                                    if **s == QUOTE {
                                        self.escaped = true;
                                        will_reset_escaped = true;
                                        continue;
                                    }
                                }
                                if self.quoted && !self.escaped {
                                    self.quoted = false;
                                    continue;
                                }
                                bytes.push(*byte);
                            }
                            b',' => {
                                consume += 1;
                                if self.escaped || self.quoted {
                                    bytes.push(*byte);
                                } else {
                                    break;
                                }
                            }
                            b'\n' | b'\r' => {
                                if self.escaped || self.quoted {
                                    bytes.push(*byte);
                                    consume += 1;
                                } else {
                                    break;
                                }
                            }
                            _ => {
                                bytes.push(*byte);
                                consume += 1;
                            }
                        }
                        if will_reset_escaped {
                            self.escaped = false;
                            will_reset_escaped = false;
                            continue;
                        }
                    }
                }
            }
            (consume, bytes, err)
        };
        self.reader.consume(consumed);
        if let Some(e) = err {
            return Err(e);
        }
        Ok(String::from_utf8_lossy(&bytes).into_owned())
    }
}

impl<T: BufRead> Iterator for Parser<T> {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Vec<String>> {
        if self.eof {
            return None;
        }
        let mut row: Vec<String> = vec![];
        if let Ok(first) = self.next_part() {
            row.push(first);
        } else {
            return None;
        }
        while let Ok(part) = self.next_part() {
            row.push(part);
        }
        Some(row)
    }
}
